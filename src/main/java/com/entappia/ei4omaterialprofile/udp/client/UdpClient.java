package com.entappia.ei4omaterialprofile.udp.client;

public interface UdpClient {
	public void sendMessage(String message);
}
