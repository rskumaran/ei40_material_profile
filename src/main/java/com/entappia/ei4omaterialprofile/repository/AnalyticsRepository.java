package com.entappia.ei4omaterialprofile.repository;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4omaterialprofile.dbmodels.Location;
import com.entappia.ei4omaterialprofile.utils.Utils;

@Component
public class AnalyticsRepository {
	
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	public List<?> SelectTableNamesLike(String tableName)
	{
		List<?> TableNames = null;
		String sqlQueryString = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES where table_name like '" + tableName + "%'";
		Utils.printInConsole(sqlQueryString);
		
		Session session = null;
		try {

			
			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);

			TableNames = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return TableNames;
		}finally {
			if (session!= null)
				session.close();
		} 

		return TableNames;
		
	}
	public List<Location> SelectAll(String tableName)
	{
		List<Location> locationRecords = null;
		String sqlQueryString = "SELECT * FROM `" + tableName + "`";
		Utils.printInConsole(sqlQueryString);
		
		Session session = null;
		try {
			
			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);
				
			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			e.printStackTrace();
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;
		
	}
	public List<Location> SelectByUserId(String tableName, String userId)
	{
		List<Location> locationRecords = null;
		String sqlQueryString = "SELECT * FROM `" + tableName + "` where id = '" + userId + "' ";
		Utils.printInConsole(sqlQueryString);
		
		Session session = null;
		try {

			
			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);
				
			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;
		
	}
	public List<?> SelectHoursByEmployee(String tableName)
	{
		List<?> locationRecords = null;
		String sqlQueryString = "SELECT * FROM `" + tableName + "`";
		Utils.printInConsole(sqlQueryString);
		
		Session session = null;
		try {

			
			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);

			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;
		
	}
	public List<Location> SelectAllAssetsByAssetId(String tableName, String assetId, String innertableName,
			String assetType) {
		
		List<Location> locationRecords = null;
		String sqlQueryString = "";
		if(Utils.isEmptyString(assetType))
			sqlQueryString = "SELECT * FROM `" + tableName + "` where id = '" + assetId + "'";
		else
			sqlQueryString = "SELECT * FROM `" + tableName + "` where id = '" + assetId + "' and id in (select asset_id from " + innertableName + " where type like '"+ assetType + "')";
		
		Utils.printInConsole(sqlQueryString);
		
		Session session = null;
		try {

			
			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);
				
			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;
	}
	public List<Location> SelectAllAssets(String tableName, String innertableName, String assetType) {

		List<Location> locationRecords = null;
		//String sqlQueryString = "SELECT * FROM `" + tableName + "`";
		
		String sqlQueryString = "";
		if(Utils.isEmptyString(assetType))
			sqlQueryString = "SELECT * FROM `" + tableName + "`";
		else
			sqlQueryString = "SELECT * FROM `" + tableName + "` where id in (select asset_id from " + innertableName + " where type like '"+ assetType + "')";

		Utils.printInConsole(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);

			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			e.printStackTrace();
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;
	}
	public List<Location> Select(String tableName, String innertableName, String assetType) {

		List<Location> locationRecords = null;
		//String sqlQueryString = "SELECT * FROM `" + tableName + "`";

		String sqlQueryString = "SELECT * FROM `" + tableName + "` where id in (select asset_id from " + innertableName + " where type like '"+ assetType + "')";

		Utils.printInConsole(sqlQueryString);

		Session session = null;
		try {

			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);

			locationRecords = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			e.printStackTrace();
			return locationRecords;
		}finally {
			if (session!= null)
				session.close();
		} 

		return locationRecords;
	}
	/*select sum(total) as time, id, zone, zonename
	from (
	    select count(zone) as total, id, zone, zone_name as zonename from table_name_1
	    group by zone,id,zone_name

	    union 

	    select count(zone) as total, id, zone, zone_name as zonename from table_name_2
	    group by zone, id, zone_name
	) as aliasT
	group by zone, id, zonename*/
	
	
	public List<Location> SelectGroupByWorkOrderZone(List<String> tableNames) {
		List<Location> locationRecords = null;

		if(null != tableNames && tableNames.size() > 0) {
			
			String sqlQueryString = "select min(time) as time, sum(duration) as duration, max(grid) as grid, grid_Pos, id, zone, zone_Name from ( ";
			
			for(int j=0;j<tableNames.size();j++){

				Utils.printInConsole("In Between Table Names : " + tableNames.get(j));

				sqlQueryString += " select min(time) as time, sum(duration) as duration, max(time) as grid, grid_Pos as grid_Pos, id, zone, zone_Name as zone_Name from `" +
						tableNames.get(j) + "` group by zone, id, zone_Name, grid, grid_Pos ";

				//If it is the final table dont append union 
				//append alias variable and loop will end
				// append other query variables after the loop ends
				
				if(j == tableNames.size() - 1) {// If equal append alias name
					sqlQueryString += " ) as aliasT " ;
				}else{
					sqlQueryString += " union " ;
				}
			}
			
			
			
			sqlQueryString += " group by zone, id, zone_Name, grid, grid_Pos";
			
			Utils.printInConsole("Query : " + sqlQueryString);
			
			Session session = null;
			
			try {

				session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

				Transaction transaction = session.beginTransaction();

				Query<Location> fetchQuery = session.createNativeQuery(sqlQueryString, Location.class);

				locationRecords = fetchQuery.getResultList();
				transaction.commit();

				if (session!= null)
					session.close();

			}catch (Exception e) {
				e.printStackTrace();
				return locationRecords;
			}finally {
				if (session!= null)
					session.close();
			}

		}




		return locationRecords;
	}



}
