package com.entappia.ei4omaterialprofile.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4omaterialprofile.dbmodels.WorkOrderZoneAnalysis;

@Repository
public interface WorkOrderZoneAnalysisRepository extends CrudRepository<WorkOrderZoneAnalysis, Integer> {

	WorkOrderZoneAnalysis findByOrderIdAndStationId(String orderNo, String stationId);
	List<WorkOrderZoneAnalysis> findByOrderId(String orderNo);

}
