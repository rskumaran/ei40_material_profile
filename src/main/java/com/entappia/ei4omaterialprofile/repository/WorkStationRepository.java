package com.entappia.ei4omaterialprofile.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4omaterialprofile.dbmodels.WorkStation;

@Repository
public interface WorkStationRepository extends CrudRepository<WorkStation, Integer> {

	@Query(value = "select * from work_station where active = 1 ", nativeQuery = true)
	Iterable<WorkStation> getAllactiveWorkStation();
	
	//SELECT * FROM work_station WHERE 28 IN (outbound,inbound,work_area,storage) and active = 1;
	@Query(value = "select * from work_station where :ZoneId IN (outbound,inbound,work_area,storage) and active = 1 ", nativeQuery = true)
	Iterable<WorkStation> getAllactiveWorkStationwithZone(@Param("ZoneId") int zoneId);

}
