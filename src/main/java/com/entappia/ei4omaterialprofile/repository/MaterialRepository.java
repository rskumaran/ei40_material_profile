package com.entappia.ei4omaterialprofile.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4omaterialprofile.dbmodels.Material;
import com.entappia.ei4omaterialprofile.dbmodels.Tags;

@Repository
public interface MaterialRepository  extends CrudRepository<Material, Integer> {

	//Iterable<Material> findByActive(boolean activeStatus);
	
	@Query(value = "select * from Material where active = 1 ", nativeQuery = true)
	Iterable<Material>  getAllactiveMaterials();
	
}
