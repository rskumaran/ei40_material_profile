package com.entappia.ei4omaterialprofile.utils;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entappia.ei4omaterialprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4omaterialprofile.dbmodels.MaterialLogs;
import com.entappia.ei4omaterialprofile.repository.MaterialLogsRepository;




@Service
public class LogEvents {

	@Autowired
	MaterialLogsRepository materialLogsRepository;

	public synchronized void addLogs(LogEventType type, String major, String minor, String message) {
		if (materialLogsRepository != null) {
			MaterialLogs logs = new MaterialLogs();
			logs.setDate(new Date());

			if (LogEventType.ERROR == type)
				logs.setType("Error");
			else
				logs.setType("Event");
			logs.setMajor(major);
			logs.setMinor(minor);
			logs.setResult(message);
			materialLogsRepository.save(logs);
		}
	}

}
