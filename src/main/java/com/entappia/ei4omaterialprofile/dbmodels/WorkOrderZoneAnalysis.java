package com.entappia.ei4omaterialprofile.dbmodels;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.entappia.ei4omaterialprofile.constants.AppConstants;
import com.entappia.ei4omaterialprofile.dbmodels.idclass.WorkOrderZoneAnalysisIdClass;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "WorkOrderZoneAnalysis")
@IdClass(WorkOrderZoneAnalysisIdClass.class)

public class WorkOrderZoneAnalysis {

	@Id
	@Column(length = 20)
	String orderId;

	@Id
	@Column(length = 12)
	String stationId;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MMM-dd HH:mm")
	Date recordCreatedDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MMM-dd HH:mm")
	Date inboundStartTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MMM-dd HH:mm")
	Date outboundStartTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MMM-dd HH:mm")
	Date workAreaStartTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MMM-dd HH:mm")
	Date storageStartTime;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MMM-dd HH:mm")
	Date inboundEndTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MMM-dd HH:mm")
	Date outboundEndTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MMM-dd HH:mm")
	Date workAreaEndTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "yyyy-MMM-dd HH:mm")
	Date storageEndTime;

	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getStationId() {
		return stationId;
	}
	public void setStationId(String stationId) {
		this.stationId = stationId;
	}
	public Date getInboundStartTime() {
		return inboundStartTime;
	}
	public void setInboundStartTime(Date inboundStartTime) {
		this.inboundStartTime = inboundStartTime;
	}
	public Date getOutboundStartTime() {
		return outboundStartTime;
	}
	public void setOutboundStartTime(Date outboundStartTime) {
		this.outboundStartTime = outboundStartTime;
	}
	public Date getWorkAreaStartTime() {
		return workAreaStartTime;
	}
	public void setWorkAreaStartTime(Date workAreaStartTime) {
		this.workAreaStartTime = workAreaStartTime;
	}
	public Date getStorageStartTime() {
		return storageStartTime;
	}
	public void setStorageStartTime(Date storageStartTime) {
		this.storageStartTime = storageStartTime;
	}
	public Date getInboundEndTime() {
		return inboundEndTime;
	}
	public void setInboundEndTime(Date inboundEndTime) {
		this.inboundEndTime = inboundEndTime;
	}
	public Date getOutboundEndTime() {
		return outboundEndTime;
	}
	public void setOutboundEndTime(Date outboundEndTime) {
		this.outboundEndTime = outboundEndTime;
	}
	public Date getWorkAreaEndTime() {
		return workAreaEndTime;
	}
	public void setWorkAreaEndTime(Date workAreaEndTime) {
		this.workAreaEndTime = workAreaEndTime;
	}
	public Date getStorageEndTime() {
		return storageEndTime;
	}
	public void setStorageEndTime(Date storageEndTime) {
		this.storageEndTime = storageEndTime;
	}
	public Date getRecordCreatedDate() {
		return recordCreatedDate;
	}
	public void setRecordCreatedDate(Date recordCreatedDate) {
		this.recordCreatedDate = recordCreatedDate;
	}

}
