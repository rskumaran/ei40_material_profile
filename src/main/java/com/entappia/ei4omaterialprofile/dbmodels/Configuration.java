package com.entappia.ei4omaterialprofile.dbmodels;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Configuration")
public class Configuration {

	@Id
	@Column(length = 37)
	String orgId;

	@Column(nullable = false, columnDefinition = "SMALLINT")
	int logAge;
	@Column(nullable = false, columnDefinition = "SMALLINT")
	int transactionDataAge;

	@Column(length = 20)
	String timeZone;
	@Column(length = 8)
	String staffTagColor;
	@Column(length = 8)
	String visitorTagColor;
	@Column(length = 8)
	String gageTagColor;
	@Column(length = 8)
	String toolTagColor;
	@Column(length = 8)
	String fixtureTagColor;
	@Column(length = 8)
	String partTagColor;
	@Column(length = 8)
	String materialTagColor;
	@Column(length = 8)
	String rmaTagColor;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public int getLogAge() {
		return logAge;
	}

	public void setLogAge(int logAge) {
		this.logAge = logAge;
	}

	public int getTransactionDataAge() {
		return transactionDataAge;
	}

	public void setTransactionDataAge(int transactionDataAge) {
		this.transactionDataAge = transactionDataAge;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getStaffTagColor() {
		return staffTagColor;
	}

	public void setStaffTagColor(String staffTagColor) {
		this.staffTagColor = staffTagColor;
	}

	public String getVisitorTagColor() {
		return visitorTagColor;
	}

	public void setVisitorTagColor(String visitorTagColor) {
		this.visitorTagColor = visitorTagColor;
	}

	public String getGageTagColor() {
		return gageTagColor;
	}

	public void setGageTagColor(String gageTagColor) {
		this.gageTagColor = gageTagColor;
	}

	public String getToolTagColor() {
		return toolTagColor;
	}

	public void setToolTagColor(String toolTagColor) {
		this.toolTagColor = toolTagColor;
	}

	public String getFixtureTagColor() {
		return fixtureTagColor;
	}

	public void setFixtureTagColor(String fixtureTagColor) {
		this.fixtureTagColor = fixtureTagColor;
	}

	public String getPartTagColor() {
		return partTagColor;
	}

	public void setPartTagColor(String partTagColor) {
		this.partTagColor = partTagColor;
	}

	public String getMaterialTagColor() {
		return materialTagColor;
	}

	public void setMaterialTagColor(String materialTagColor) {
		this.materialTagColor = materialTagColor;
	}

	public String getRmaTagColor() {
		return rmaTagColor;
	}

	public void setRmaTagColor(String rmaTagColor) {
		this.rmaTagColor = rmaTagColor;
	}

}
