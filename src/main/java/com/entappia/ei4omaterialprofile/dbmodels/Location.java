package com.entappia.ei4omaterialprofile.dbmodels;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


import com.entappia.ei4omaterialprofile.dbmodels.idclass.LocationIdClass;


@Entity
@Table(name = "Location")
@IdClass(LocationIdClass.class)
public class Location {
	
	@Id
	 String id;
	 String zone;
	 @Id
	 int time;
	 int grid;
	 String gridPos;
	 String zoneName;
	 int duration;
	 
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getId() {
		return id;
	}
	public void setStaffId(String id) {
		this.id = id;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getGrid() {
		return grid;
	}
	public void setGrid(int grid) {
		this.grid = grid;
	}
	public String getGridPos() {
		return gridPos;
	}
	public void setGridPos(String gridPos) {
		this.gridPos = gridPos;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	 

}
