package com.entappia.ei4omaterialprofile.datacollection;

import java.util.Date;

import com.entappia.ei4omaterialprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4omaterialprofile.constants.AppConstants.AssignmentType;;

public class Notification {
	
	String workOrderId;//Chrono or non chrono
	String tagId;//The Id assigned for the asset
	Date lastNotifiedTime;
	String Notification;// this string is to map the condition with the udp message type will be needed later
	NotificationType errorType;
	int notificationCount = 0;
	String NotificationStatus;// this is to display in the mail or message as status
	Date lastOccuredTime;
	boolean sendNotification;
	String zoneName;
	
	public String getWorkOrderId() {
		return workOrderId;
	}
	public void setWorkOrderId(String workOrderId) {
		this.workOrderId = workOrderId;
	}
	public String getTagId() {
		return tagId;
	}
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	public Date getLastNotifiedTime() {
		return lastNotifiedTime;
	}
	public void setLastNotifiedTime(Date lastNotifiedTime) {
		this.lastNotifiedTime = lastNotifiedTime;
	}
	public String getNotification() {
		return Notification;
	}
	public void setNotification(String notification) {
		Notification = notification;
	}
	public NotificationType getErrorType() {
		return errorType;
	}
	public void setErrorType(NotificationType errorType) {
		this.errorType = errorType;
	}
	public String getNotificationStatus() {
		return NotificationStatus;
	}
	public void setNotificationStatus(String notificationStatus) {
		NotificationStatus = notificationStatus;
	}
	public Date getLastOccuredTime() {
		return lastOccuredTime;
	}
	public void setLastOccuredTime(Date lastOccuredTime) {
		this.lastOccuredTime = lastOccuredTime;
	}
	public boolean isSendNotification() {
		return sendNotification;
	}
	public void setSendNotification(boolean sendNotification) {
		this.sendNotification = sendNotification;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public int getNotificationCount() {
		return notificationCount;
	}
	public void setNotificationCount(int notificationCount) {
		this.notificationCount = notificationCount;
	}
}
