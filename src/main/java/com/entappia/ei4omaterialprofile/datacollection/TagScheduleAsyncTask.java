package com.entappia.ei4omaterialprofile.datacollection;

import java.sql.Time;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4omaterialprofile.constants.AppConstants.AssignmentType;
import com.entappia.ei4omaterialprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4omaterialprofile.constants.AppConstants.ZoneType;
import com.entappia.ei4omaterialprofile.dbmodels.CampusDetails;
import com.entappia.ei4omaterialprofile.dbmodels.MaterialLogs;
import com.entappia.ei4omaterialprofile.dbmodels.Shift;
import com.entappia.ei4omaterialprofile.dbmodels.Tags;
import com.entappia.ei4omaterialprofile.dbmodels.WorkOrderAnalysis;
import com.entappia.ei4omaterialprofile.dbmodels.WorkOrderZoneAnalysis;
import com.entappia.ei4omaterialprofile.dbmodels.WorkStation;
import com.entappia.ei4omaterialprofile.dbmodels.ZoneDetails;
import com.entappia.ei4omaterialprofile.quuppa.QuuppaApiService;
import com.entappia.ei4omaterialprofile.repository.AnalyticsRepository;
import com.entappia.ei4omaterialprofile.repository.CampusDetailsRepository;
import com.entappia.ei4omaterialprofile.repository.MaterialLogsRepository;
import com.entappia.ei4omaterialprofile.repository.ShiftRepository;
import com.entappia.ei4omaterialprofile.repository.TableUtils;
import com.entappia.ei4omaterialprofile.repository.TagsRepository;
import com.entappia.ei4omaterialprofile.repository.WorkOrderAnalysisRepository;
import com.entappia.ei4omaterialprofile.repository.WorkOrderZoneAnalysisRepository;
import com.entappia.ei4omaterialprofile.repository.WorkStationRepository;
import com.entappia.ei4omaterialprofile.repository.ZoneRepository;
import com.entappia.ei4omaterialprofile.udp.client.UdpClient;
import com.entappia.ei4omaterialprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4omaterialprofile.utils.LogEvents;
import com.entappia.ei4omaterialprofile.utils.Preference;
import com.google.gson.Gson;
import com.entappia.ei4omaterialprofile.utils.Utils;
import com.entappia.ei4omaterialprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4omaterialprofile.dbmodels.Location;
import com.entappia.ei4omaterialprofile.dbmodels.ProfileConfiguration;
import com.entappia.ei4omaterialprofile.models.ConfigData;
import com.entappia.ei4omaterialprofile.repository.ProfileConfigurationRepository;
import com.entappia.ei4omaterialprofile.constants.AppConstants;
import com.snatik.polygon.Point;
import com.snatik.polygon.Polygon;


@Component
public class TagScheduleAsyncTask {

	public String TAG_NAME = "TagScheduleTask";

	Preference preference = new Preference();

	@Autowired
	private QuuppaApiService quuppaApiService;

	@Autowired
	private TagsRepository tagsRepository;
	
	@Autowired
	private WorkStationRepository workStationRepository;

	@Autowired
	private WorkOrderAnalysisRepository workOrderAnalysisRepository;
	
	@Autowired
	private WorkOrderZoneAnalysisRepository workOrderZoneAnalysisRepository;
	
	@Autowired
	private ProfileConfigurationRepository profileConfigurationRepository;
	
	@Autowired
	private AnalyticsRepository analyticsRepository;
	
	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	@Autowired
	private ZoneRepository zoneRepository;
	
	@Autowired
	private MaterialLogsRepository materialLogsRepository;
	

	@Autowired
	private LogEvents logEvents;


	@Autowired
	private ShiftRepository shiftRepository;
	
	@Autowired
	private TableUtils tableutil;

	private final UdpClient udpClient;

	@Autowired
	private NotificationManager notificationManager ;

	//move these functions to a separate class
	//
	private HashMap<String, List<String>> gridZoneMap;
	
	private HashMap<String, HashMap<String, Object> > workStationBounds;
	
	private JSONArray tagsJsonArray = null;
	private CampusDetails campusDetails = null;
	private JSONArray smoothedPosition = null;
	
	int maxDaysToKeepOldTables = AppConstants.maxDaysToKeepOldTables;
	int threadTimeDuration = AppConstants.threadTimeDuration;
	int maxDaysToKeepOldLogData = AppConstants.maxDaysToKeepOldLogData;
	
	LocalDate tablesDeletedDate = null;
	boolean deleteOldTables = true;
	LocalDate logsDeletedDate = null;
	boolean deleteOldLogs = true;
	
	private ScheduledExecutorService scheduler;
	private ScheduledFuture<?> futureTask = null;
	private Runnable myTask;
	
	int count = 0;

	@Autowired
	public TagScheduleAsyncTask(UdpIntegrationClient udpClient) {
		this.udpClient = udpClient;
		workStationBounds = new HashMap<>();

	}

	@PostConstruct
	private void intializeThread()
	{
		scheduler =	 Executors.newScheduledThreadPool(2);
		
		myTask = new Runnable()
		{
			@Override
			public void run()
			{
				getTagPositions();
			}
		};
		if(threadTimeDuration > 0)
	    {       
	        if (futureTask != null)
	        {
	            futureTask.cancel(true);
	        }

	        futureTask = scheduler.scheduleAtFixedRate(myTask, 0, threadTimeDuration * 60, TimeUnit.SECONDS);
	    }
		
	}
	int getReadInterval()
	{
		//read here from repository for changes in thread timing and return
		//need to check if 0 the thread has to be stopped or not
		ProfileConfiguration profileConfiguration = profileConfigurationRepository.selectTopRecord();
		int threadTimeDurationLocal = -1;
		if(null != profileConfiguration) {

			List<ConfigData> configDataList = profileConfiguration.getConfigData();
			if (configDataList != null && configDataList.size() > 0) {
				for (int i = 0; i < configDataList.size(); i++) {

					ConfigData configData = configDataList.get(i);

					if (configData != null) {
						 if (configData.getName().equals(AppConstants.CONFIG_MAT)) {
							threadTimeDurationLocal = Integer.parseInt(configData.getDefaultval());
							if(threadTimeDurationLocal <= 0)
							{
								threadTimeDurationLocal = -1;
							}
							Utils.printInConsole("ThreadTimeDuration : "+ threadTimeDurationLocal);
						}
					}
				}
			}
		}

	
		return threadTimeDurationLocal;
	}
	public void checkAndChangeReadInterval()
	{
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {
			public void run() {
				int currentReadInterval = getReadInterval();
				Utils.printInConsole("current thread Interval" + currentReadInterval);
				Utils.printInConsole("threadTimeDuration" + threadTimeDuration);
				if(-1 != currentReadInterval) {
					if(currentReadInterval != threadTimeDuration) {
						threadTimeDuration = currentReadInterval;
						changeReadInterval(threadTimeDuration * 60);
					}
				}
			}
		});
		executorService.shutdown();
	}
	public void changeReadInterval(long time)
	{
	    if(time > 0)
	    {       
	        if (futureTask != null)
	        {
	            futureTask.cancel(true);
	        }

	        futureTask = scheduler.scheduleAtFixedRate(myTask, time, time, TimeUnit.SECONDS);
	    }
	}
	
	Gson gson = new Gson();
	DecimalFormat df = new DecimalFormat("#.##");

	private boolean getTagListFromDevice()
	{
		try {
			tagsJsonArray = null;
			CompletableFuture<JSONObject> getTagCompletableFuture = quuppaApiService.getTagDetails();
			CompletableFuture.allOf(getTagCompletableFuture).join();

			if (getTagCompletableFuture.isDone()) {
				preference.setLongValue("lastscan", System.currentTimeMillis());

				JSONObject jsonObject = getTagCompletableFuture.get();
				if (jsonObject != null) {

					String status = jsonObject.optString("status");

					if (!Utils.isEmptyString(status)) {
						if (status.equals("success")) {

							JSONObject jsonTagObject = jsonObject.optJSONObject("response");
							if (jsonTagObject != null) {
								int code = jsonTagObject.getInt("code");
								if (code == 0) {
									tagsJsonArray = jsonTagObject.getJSONArray("tags");
								}
							}
						}
						else if (status.equals("error")) {
							String message = jsonObject.getString("message");
							logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule", message);

						}
					}
				}
			}
		}
		catch(Exception e) {
			logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule", e.getMessage());
			e.printStackTrace();
		}
		if(tagsJsonArray!= null)
			return true;
		return false;
	}
	private boolean getCampusDetails() {
		campusDetails = null;
		campusDetails = getCampusDetails(1);
		if(campusDetails != null) {
			return true;
		}
		return false;
	}
	private boolean searchTag(String macId) {
		smoothedPosition = null;
		for(int i=0; i<tagsJsonArray.length(); i++){
			JSONObject tagJsonObject = tagsJsonArray.getJSONObject(i);
			String id = tagJsonObject.optString("tagId", "");

			if(macId.equals(id))
			{
				smoothedPosition = tagJsonObject
						.optJSONArray("location");
				if (smoothedPosition != null)
					return true;
				else
					return false;
			}
		}

		return false;
	}
	private double getSmoothedPositionX() {
		if (smoothedPosition != null)
			return smoothedPosition.getDouble(0);
		return -1;
	}
	private double getSmoothedPositionY() {
		if (smoothedPosition != null)
			return smoothedPosition.getDouble(1);
		return -1;
	}
	private boolean getGridZoneMap() {

		gridZoneMap = new HashMap<>();
		if (campusDetails != null)
			gridZoneMap = campusDetails.getGridZoneDetails();
		else
			return false;
		if(gridZoneMap!= null) {
			return true;
		}
		return false;
	}
	private double getOriginX() {
		if(campusDetails!= null)
			return campusDetails.getOriginX()
					* campusDetails.getMetersPerPixelX();
		return -1;
	}
	private double getOriginY() {
		if(campusDetails!= null)
			return campusDetails.getOriginY()
					* campusDetails.getMetersPerPixelY();
		return -1;
	}
private void checkWorkStationBounds() {

	String stationId = "";

	List<WorkStation> workStationList = (List<WorkStation>) workStationRepository.getAllactiveWorkStation();

	WorkStation workStation = null;

	for (int index = 0; index < workStationList.size(); index++) {
		workStation = workStationList.get(index);
		stationId = workStation.getStationId();

		if (workStationBounds.containsKey(stationId)) {
			HashMap<String, Object> stationMap = workStationBounds.get(stationId);
			if (stationMap.containsKey("inbound")) {
				Integer count = (Integer) stationMap.get("inbound");
				Integer inboundlimit = workStation.getInLimit();
				if (count > inboundlimit) {
					//send notification
					notificationManager.addNotification(stationId, "Inbound", "Limitcrossed", NotificationType.EMERGENCY, workStation.getInbound().getName());
				}else {
					//clear notification
					notificationManager.removeNotification(stationId, "Inbound", "Limitcrossed");
				}
			}

			if (stationMap.containsKey("outbound")) {
				Integer count = (Integer) stationMap.get("outbound");
				Integer outboundlimit = workStation.getOutLimit();
				if (count > outboundlimit) {
					//send notification
					notificationManager.addNotification(stationId, "Outbound", "Limitcrossed", NotificationType.EMERGENCY, workStation.getOutbound().getName());
				}else {
					//clear notification
					notificationManager.removeNotification(stationId, "Outbound", "Limitcrossed");
	}
			}
		}
	}
}

	private void AddWorkStationData(String orderId, int zone, String zoneName ){

		String stationId = "";
		
		List<WorkStation> workStationList = (List<WorkStation>) workStationRepository.getAllactiveWorkStationwithZone(zone);

		boolean inbound = false;
		boolean outbound = false;

		WorkStation workStation = null;

		for(int index = 0; index < workStationList.size();index++) {
			workStation = workStationList.get(index);
			int zoneId = zone;
			if(zoneId == workStation.getInbound().getId()) {
				inbound = true;
				stationId = workStation.getStationId();
			}else if(zoneId == workStation.getOutbound().getId()) {
				outbound = true;
				stationId = workStation.getStationId();
			}
			
			if(!Utils.isEmptyString(stationId)) {
				
		
				if(workStationBounds.containsKey(stationId))
				{
					HashMap<String, Object> stationMap = workStationBounds.get(stationId);
					if(true == inbound) {
						
						if(stationMap.containsKey("inbound"))
						{
							Integer count = (Integer) stationMap.get("inbound");
							stationMap.put("inbound", count+1);
						}
						else
						{
							stationMap.put("inbound", 1);
							stationMap.put("inboundzone", zoneName);
						}
						workStationBounds.put(stationId, stationMap);
					}
					else if(true == outbound) {
						
						if(stationMap.containsKey("outbound"))
						{
							Integer count = (Integer) stationMap.get("outbound");
							stationMap.put("outbound", count+1);
						}
						else
						{
							stationMap.put("outbound", 1);
							stationMap.put("outboundzone", zoneName);
						}
						workStationBounds.put(stationId, stationMap);
					} 

				}else
				{
					HashMap<String, Object> stationMap = new HashMap<>();//workStationBounds.get(stationId);
					if(true == inbound) {
						
						stationMap.put("inbound", 1);
						workStationBounds.put(stationId, stationMap);
						stationMap.put("inboundzone", zoneName);
					}
					else if(true == outbound) {
						
						stationMap.put("outbound", 1);
						workStationBounds.put(stationId, stationMap);
						stationMap.put("outboundzone", zoneName);
					} 

				}
				
				
				workStationBounds.get(stationId);
				
			}//If stationId not empty
	
		}
}
	
	private void parseWorkOrderAnalysisData(String orderId, int zone, int duration, String tagId, String zoneName  ){

		Date analysisDate = new Date();
		String stationId = "";
		List<String> stationIdList = new ArrayList<String>();

		List<WorkStation> workStationList = (List<WorkStation>) workStationRepository.getAllactiveWorkStationwithZone(zone);

		boolean inbound = false;
		boolean outbound = false;
		boolean workarea = false;
		boolean storagearea = false;

		WorkStation workStation = null;

		for(int index = 0; index < workStationList.size();index++) {
			workStation = workStationList.get(index);
			int zoneId = zone;
			if(zoneId == workStation.getInbound().getId()) {
				inbound = true;
				stationId = workStation.getStationId();
			}else if(zoneId == workStation.getOutbound().getId()) {
				outbound = true;
				stationId = workStation.getStationId();
			}else if(zoneId == workStation.getStorage().getId()) {
				//Send shipping Notification Here

				notificationManager.addNotification(orderId, tagId, "ShippingArea",NotificationType.EMERGENCY, zoneName);

				logEvents.addLogs(LogEventType.EVENT, "Run", "Run-MaterialprofileApplication",
						"WorkOrder : " + orderId + " Tag : " + tagId + " is found in shipping Area " + zoneName + " zone");
						//"Lost or not working or expired tag detected MacId: " + LostTag.getMacId());
				storagearea = true;
				stationId = workStation.getStationId();
			}else if(zoneId == workStation.getWorkArea().getId()) {
				workarea = true;
				stationId = workStation.getStationId();
			}



			if(!Utils.isEmptyString(stationId)) {
				//Add station Id to list here
				//This is used below after completion of this loop

				stationIdList.add(stationId);

				//Add here the code for analysis data
				WorkOrderZoneAnalysis workOrderZoneAnalysis = workOrderZoneAnalysisRepository.findByOrderIdAndStationId(orderId, stationId);

				if(null == workOrderZoneAnalysis) {

					workOrderZoneAnalysis = new WorkOrderZoneAnalysis();
					//insert primary key values
					workOrderZoneAnalysis.setOrderId(orderId);
					workOrderZoneAnalysis.setStationId(stationId);
					workOrderZoneAnalysis.setRecordCreatedDate(analysisDate);
				}

				//Grid variable is used for max time
				//need to change
				if(true == inbound) {
					if(workOrderZoneAnalysis.getInboundStartTime() == null)
						workOrderZoneAnalysis.setInboundStartTime(analysisDate);
					/*if(workOrderZoneAnalysis.getInboundEndTime() != null)
						workOrderZoneAnalysis.setInboundEndTime(null);*/
				}else if(true == outbound) {
					
					if(workOrderZoneAnalysis.getWorkAreaEndTime() == null && workOrderZoneAnalysis.getWorkAreaStartTime() != null)
						workOrderZoneAnalysis.setWorkAreaEndTime(analysisDate);
					
					if(workOrderZoneAnalysis.getOutboundStartTime() == null && workOrderZoneAnalysis.getWorkAreaEndTime() != null && workOrderZoneAnalysis.getWorkAreaStartTime() != null)
						workOrderZoneAnalysis.setOutboundStartTime(analysisDate);
					/*if(workOrderZoneAnalysis.getOutboundWorkOrderEndTime() != null)
						workOrderZoneAnalysis.setOutboundWorkOrderEndTime(null);*/
					
				}else if(true == workarea) {
					
					if(workOrderZoneAnalysis.getInboundEndTime() == null && workOrderZoneAnalysis.getInboundStartTime() != null)
						workOrderZoneAnalysis.setInboundEndTime(analysisDate);
					
					if(workOrderZoneAnalysis.getWorkAreaStartTime() == null && workOrderZoneAnalysis.getInboundEndTime() != null && workOrderZoneAnalysis.getInboundStartTime() != null)
						workOrderZoneAnalysis.setWorkAreaStartTime(analysisDate);
					/*if(workOrderZoneAnalysis.getWorkAreaWorkOrderEndTime() != null)
						workOrderZoneAnalysis.setWorkAreaWorkOrderEndTime(null);*/
					
				}else if(true == storagearea) {
					
					/*if(workOrderZoneAnalysis.getStorageWorkOrderEndTime() != null)
						workOrderZoneAnalysis.setStorageWorkOrderEndTime(null);*/

					if(workOrderZoneAnalysis.getOutboundEndTime() == null && workOrderZoneAnalysis.getOutboundStartTime() != null)
						workOrderZoneAnalysis.setOutboundEndTime(analysisDate);

					if(workOrderZoneAnalysis.getWorkAreaEndTime() == null && workOrderZoneAnalysis.getWorkAreaStartTime() != null)
						workOrderZoneAnalysis.setWorkAreaEndTime(analysisDate);
					
					if(workOrderZoneAnalysis.getStorageStartTime() == null && workOrderZoneAnalysis.getOutboundEndTime() != null && workOrderZoneAnalysis.getOutboundStartTime() != null)
						workOrderZoneAnalysis.setStorageStartTime(analysisDate);

				}else {
					//This else case will not be executed check again
					//Never in this else case
					//something to do later
					if(workOrderZoneAnalysis.getWorkAreaEndTime() == null && workOrderZoneAnalysis.getWorkAreaStartTime() != null)
						workOrderZoneAnalysis.setWorkAreaEndTime(analysisDate);
				}

				workOrderZoneAnalysisRepository.save(workOrderZoneAnalysis);
			}//If stationId empty
		}

		//Add here the code for analysis data
		List<WorkOrderZoneAnalysis> workOrderZoneAnalysisList = workOrderZoneAnalysisRepository.findByOrderId(orderId);

		for(int i=0; i < workOrderZoneAnalysisList.size(); i++ ) {

			WorkOrderZoneAnalysis workOrderZoneAnalysis = workOrderZoneAnalysisList.get(i);

			//stationId variable is used temporarily above so it is reused
			stationId = workOrderZoneAnalysis.getStationId();

			if(false == stationIdList.contains(stationId)) {

				if(workOrderZoneAnalysis.getInboundStartTime() != null && workOrderZoneAnalysis.getInboundEndTime() != null) {
					if(workOrderZoneAnalysis.getWorkAreaStartTime() != null  && workOrderZoneAnalysis.getWorkAreaEndTime() != null) {
						if(workOrderZoneAnalysis.getOutboundEndTime() == null && workOrderZoneAnalysis.getOutboundStartTime() != null)
							workOrderZoneAnalysis.setOutboundEndTime(analysisDate);
					}
				}
				
				workOrderZoneAnalysisRepository.save(workOrderZoneAnalysis);
			}
		}

		
		//To do other error cases need to be handled here such as tag out of bound areas without entering into outbound
		// and tag failure cases etc How to handle? Many think.
}
			 
public void getTagPositions() {

	
		workStationBounds.clear();
		Calendar currentCalendar = Calendar.getInstance();
		currentCalendar.set(Calendar.MILLISECOND, 0);
		currentCalendar.set(Calendar.SECOND, 0);
		Date currentTime = currentCalendar.getTime();
		Utils.printInConsole("Material Run Time:-" + Utils.getFormatedDate2(new Date()), true);



				double originX = getOriginX();
				double originY = getOriginY();

				try {

					//Get current shift name
					//If there are no active shifts the shift name 
					//will be empty. quit the process or loop when no shift is 
					//found make this function call to the start of the 
					//thread and get out complete thread execution
					Utils.printInConsole("GetCurrentShiftname");
					String currentShiftName = getCurrentShiftName(currentTime);
					if(currentShiftName == "") {
						Utils.printInConsole("No shift available for current time");
						logEvents.addLogs(LogEventType.EVENT, "Run", "Run-MaterialprofileApplication",
								"No shift available for current time");
						currentShiftName = "NoShift";
						//comment this return when data need to be collected during no shift time also
						return;
					}

					//fetch all tags allocated or mapped to asset from server/DB
					//fetch from QPE one time for all the iterations
					//fetch from qpe
					Utils.printInConsole("GetTaglistfromdevice");
					if(!getTagListFromDevice()) {
						Utils.printInConsole("taglist from device not found");
						return;
					}

					Utils.printInConsole("GetCampusdetails");
					if(!getCampusDetails()) {
						Utils.printInConsole("Campusdetails Not Found");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-MaterialprofileApplication",
								"Campus Details not found");
						return;
					}
					Utils.printInConsole("GetGridzonemap");
					if(!getGridZoneMap()){
						Utils.printInConsole("Gridzone map Not Found");
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-MaterialprofileApplication",
								"Grid zone map not found");
						return;
					}
					Utils.printInConsole("GetOriginX");
					originX = getOriginX();
					if(originX == -1) {
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-MaterialprofileApplication",
								"originX not Found ");
						return;
					}
					Utils.printInConsole("GetOriginY");
					originY = getOriginY();
					if(originY == -1) {
						logEvents.addLogs(LogEventType.ERROR, "Run", "Run-MaterialprofileApplication",
								"originX not Found");
						return;
					}

					//Here we need to store subtype also in feature
					ArrayList<Tags> materialTagList = new ArrayList<Tags>();
					/*ArrayList<Material> materialList = new ArrayList<Material>();
					ArrayList<WorkStation> WorkStationList = new ArrayList<WorkStation>();*/
					
								
					materialTagList = (ArrayList<Tags>) tagsRepository.selectAssignedMaterialTags();
					/*materialList = (ArrayList<Material>) materialRepository.getAllactiveMaterials();
					WorkStationList = (ArrayList<WorkStation>) workStationRepository.getAllactiveWorkStation();*/
					
					for (int i = 0;i < materialTagList.size();i++){

						Tags materialTag = materialTagList.get(i);


						//append date and shift name as per table strategy
						Utils.printInConsole("GetCurrentTablename");
						String currentTableName = GetCurrentTableName(currentShiftName, materialTag.getType());
						if(currentTableName == "") {
							Utils.printInConsole("Table Name Formatting error");
							logEvents.addLogs(LogEventType.ERROR, "Run", "Run-MaterialprofileApplication",
									"Table Name formatting error");
							return;
						}
						Utils.printInConsole(currentTableName);
						//Check whether table exists
						//Create new table if not exists
						tableutil.createLocationTable(currentTableName);

						double x = -1;
						double y = -1;


						if(searchTag(materialTag.getMacId())) {

							x = getSmoothedPositionX();
							y = getSmoothedPositionY();

							x = Math.abs(originX - x);
							y = Math.abs(originY - y);

							//notify

						}
						else { 
							//tag not found
							logEvents.addLogs(LogEventType.ERROR, "Run", "Run-MaterialprofileApplication",
									"Tag : " + materialTag.getTagId() + " assigned to an Asset :" + materialTag.getAssignmentId() + " is missing" );
							Utils.printInConsole("tag Not Found");
							//notify
							//return;
						}
						Utils.printInConsole("GetZoneDetails");
						//check tag exists
						ZoneDetails zoneDetails = getZoneDetails(gridZoneMap, x, y);
						Utils.printInConsole("GetZoneDetails x: " + x + " y: " + y);
						if (zoneDetails != null){
							ZoneType currentZoneType = zoneDetails.getZoneType();
							String zoneName = zoneDetails.getName();

							if (zoneDetails != null && currentZoneType != ZoneType.DeadZone) {
																
								int GridNumber = (int) ((getGridPosition(x)-1) * Math.ceil(campusDetails.getWidth()) + getGridPosition(y));

								int xValue = getGridPosition(x);
								int yValue = getGridPosition(y);

								String gridPosition = xValue + "_" + yValue;

								String materialTagAssignmentID = materialTag.getAssignmentId();
								int zoneId = zoneDetails.getId();
								String materialTagID = materialTag.getTagId();
								
								int currentTimeinMinutes = Utils.getDayMintus(currentTime);

								tableutil.insertIntoLocationTable(currentTableName, materialTagAssignmentID, currentTimeinMinutes,
										GridNumber, zoneId,zoneName, gridPosition, threadTimeDuration);
								
								AddWorkStationData(materialTagAssignmentID, zoneId, zoneName);
								
								// Insert into Analysis table need to check this is needed or not 
								// For now enter all fields you can get from and they may be needed later for analysis
								parseWorkOrderAnalysisData(materialTagAssignmentID, zoneId, threadTimeDuration, materialTagID, zoneName);
					
							} else {
								//zone details not found
								Utils.printInConsole("Material is in Non Tracking Zone (Dead zone)");
								//return;
								logEvents.addLogs(LogEventType.EVENT, "Run", "Run-materialprofileApplication",
											"Material is in Non Tracking Zone (Dead zone)" );
							}

						}

						else
						{
							Utils.printInConsole("zone details not found");
							logEvents.addLogs(LogEventType.ERROR, "Run", "Run-MaterialprofileApplication",
									"ZoneDetails x: " + x + " y: " + y + "is not found" );

						}
					}
					





				}
				catch(Exception e) {
					e.printStackTrace(); 
				}

				checkWorkStationBounds();
				notificationManager.sendNotification();
				checkAndChangeReadInterval();
				checkAndModifyConfigurationData();
				modifyTableData();
	}
	private ZoneDetails getZoneDetails(HashMap<String, List<String>> gridZoneMap, double x, double y) {

		int xValue = getGridPosition(x);
		int yValue = getGridPosition(y);

		if (gridZoneMap.containsKey(xValue + "_" + yValue)) {

			List<String> zoneIdList = gridZoneMap.get(xValue + "_" + yValue);
			if (zoneIdList != null && zoneIdList.size() > 0) {
				if (zoneIdList.size() == 1) {
					String zoneId = zoneIdList.get(0);
					Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
					if (zoneDetails != null && zoneDetails.isPresent() && zoneDetails.get().isActive())
						return zoneDetails.get();
				} else {
					for (String zoneId : zoneIdList) {
						Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
						if (zoneDetails != null && zoneDetails.isPresent() && zoneDetails.get().isActive()) {

							ZoneDetails zoneDetails1 = zoneDetails.get();
							List<HashMap<String, String>> coordinateList = zoneDetails1.getCoordinateList();
							if (isInsidePolygon(coordinateList, x, y))
								return zoneDetails1;
						}
					}
				}
			}
		}

		return null;
	}

	private boolean isInsidePolygon(List<HashMap<String, String>> coordinateList, double x, double y) {
		Polygon.Builder polygonBuilder = Polygon.Builder();

		for (int j = 0; j < coordinateList.size(); j++) {
			HashMap<String, String> coordinate = coordinateList.get(j);

			double x1 = Double.valueOf(coordinate.get("x"));
			double y1 = Double.valueOf(coordinate.get("y"));

			polygonBuilder.addVertex(new Point(x1, y1));

		}

		polygonBuilder.close();

		Polygon polygon = polygonBuilder.build();
		Point point = new Point(x, y);
		boolean contains = polygon.contains(point);
		if (contains) {
			return true;
		} else
			return false;
	}

	private int getGridPosition(double num1) {
		int value = 0;
		if (num1 % 1 == 0)
			value = (int) Math.floor(num1);
		else
			value = (int) Math.floor(num1 + 1);

		return value - 1;
	}

	public CampusDetails getCampusDetails(int campusId) {
		return campusDetailsRepository.findByCampusId(campusId);
	}

	public HashMap<String, List<String>> getCampusGridList() {
		//gridZoneMap = new HashMap<>();
		//campusDetails = campusDetailsRepository.findByCampusId(1);
		if (campusDetails != null)
			gridZoneMap = campusDetails.getGridZoneDetails();

		return gridZoneMap;
	}

	// GetCurrentTableName

	// converts string according to the table name rules

	// need to handle this issue if no shift is found

	private String GetCurrentTableName(String shiftName, String tagtypeList) {

		String currentTableName = "";

		String profileName = tagtypeList + "_" + "Location";

		Date currentDate = new Date();

		// Table name hours and minutes can be used to find out in case of issues

		// For only date is used we can change it later

		// SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy HH:mm:ss");


		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");

		String scurrentDate = formatter.format(currentDate);

		currentTableName = profileName + "_" + scurrentDate + "_" + shiftName;

		return currentTableName;

	}

		//GetCurrentShiftName
			//The current working shift is needed to append in the table name 
			//to find the current working shift the current time is checked 
			//whether it lies between shift start time shift end time 
			// the issue occurs when two shifts are running in the same time
			// have to handle it 
		private String getCurrentShiftName(Date currentTime)
		{
			//Get current shift name from the shift start time and 
			ArrayList<Shift> shiftDetailsArr = (ArrayList<Shift>) shiftRepository.findActiveShifts();
			
			
		    String shiftName = "";
			for (int i = 0;i < shiftDetailsArr.size();i++){
				Shift shiftElement = shiftDetailsArr.get(i);
				Date shiftStartTime = shiftElement.getShiftStartTime();
				Date shiftEndTime = shiftElement.getShiftEndTime();
				
				Utils.printInConsole("shiftStartTime" + shiftStartTime);
				Utils.printInConsole("shiftEndTime" + shiftEndTime);
				
				Calendar calendar = Calendar.getInstance();
				calendar.clear();
		        calendar.setTime(shiftStartTime);
		        calendar.set(currentTime.getYear() + 1900,currentTime.getMonth() , currentTime.getDate());
		        shiftStartTime = calendar.getTime();
		        
		        calendar.clear();
		        calendar.setTime(shiftEndTime);
		    	calendar.set(currentTime.getYear() + 1900,currentTime.getMonth() , currentTime.getDate());
		        shiftEndTime = calendar.getTime();
		        
		        if(currentTime.after(shiftStartTime) && currentTime.before(shiftEndTime)){
					  shiftName = shiftElement.getShiftName(); break; }
		        if(currentTime.equals(shiftStartTime)){
		    		shiftName = shiftElement.getShiftName();
		    		break;
		    	}
					
			}
		
			return shiftName;
		}

	void checkAndModifyConfigurationData() {
		try {
			ProfileConfiguration profileConfiguration = profileConfigurationRepository.selectTopRecord();
			if(null != profileConfiguration) {
			List<ConfigData> configDataList = profileConfiguration.getConfigData();
			if (configDataList != null && configDataList.size() > 0) {
				for (int i = 0; i < configDataList.size(); i++) {

					ConfigData configData = configDataList.get(i);

					if (configData != null) {

						if (configData.getName().equals(AppConstants.CONFIG_TTA))
						{
							maxDaysToKeepOldTables = Integer.parseInt(configData.getDefaultval());
							if(maxDaysToKeepOldTables >= AppConstants.minDaysToKeepOldTables && maxDaysToKeepOldTables <= AppConstants.maxDaysToKeepOldTables)
							{
								if(maxDaysToKeepOldTables != AppConstants.maxDaysToKeepOldTables) {
									deleteOldTables = true;
								}
								
								if(null == tablesDeletedDate)
								{
									deleteOldTables = true;
								}
								else {//database deleted date not NULL
									LocalDate currentDate = LocalDate.now();
									if(!tablesDeletedDate.isEqual(currentDate)) {
										deleteOldTables = true;
									}
								}

							}
							else
							{
								maxDaysToKeepOldTables = AppConstants.maxDaysToKeepOldTables;
								deleteOldTables = true;
							}

						}
						else if (configData.getName().equals(AppConstants.CONFIG_LTA))
						{
							maxDaysToKeepOldLogData = Integer.parseInt(configData.getDefaultval());

							if(maxDaysToKeepOldLogData >= AppConstants.minDaysToKeepOldLogData && maxDaysToKeepOldLogData <= AppConstants.maxDaysToKeepOldLogData)
							{
								if(maxDaysToKeepOldLogData != AppConstants.maxDaysToKeepOldLogData) {
									deleteOldLogs = true;
								}
								if(null == logsDeletedDate)
								{
									deleteOldLogs = true;
								}
								else {//database deleted date not NULL
									LocalDate currentDate = LocalDate.now();
									if(!logsDeletedDate.isEqual(currentDate)) {
										deleteOldLogs = true;
									}
								}
							}
							else
							{
								maxDaysToKeepOldLogData = AppConstants.maxDaysToKeepOldLogData;
								deleteOldLogs = true;
							}

						}
						else if (configData.getName().equals(AppConstants.CONFIG_ARI)) 
						{
							int intervalBetweenNotifications = Integer.parseInt(configData.getDefaultval());
							if(intervalBetweenNotifications > 0)
							{
								notificationManager.setIntervalBetweenNotifications(intervalBetweenNotifications);
							}
							Utils.printInConsole("intervalBetweenNotifications : "+ intervalBetweenNotifications);
						}
						else if (configData.getName().equals(AppConstants.CONFIG_ARC)) {
							int maxNotificationCount = Integer.parseInt(configData.getDefaultval());
							if(maxNotificationCount >= 0)
							{
								notificationManager.setMaxNotificationCount(maxNotificationCount);
							}
							Utils.printInConsole("maxNotificationCount : "+ maxNotificationCount);
						}

					}

				}
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	void modifyTableData(){

		if(deleteOldTables) {
			LocalDate lastDateToKeepLocationTable = getLastDateToKeepLocationTable(maxDaysToKeepOldTables);
			deleteOldLocationTable(lastDateToKeepLocationTable);
			//ToDo check for if there are error conditons and set flag to false
			Utils.printInConsole("maxDaysToKeepOldTables : " + maxDaysToKeepOldTables + "Date : "+ lastDateToKeepLocationTable + "deleteOldTables : "+ deleteOldTables);
			deleteOldTables = false;
		}
		if(deleteOldLogs) {
			LocalDate lastDateToKeepLogData = getLastDateToKeepLogData(maxDaysToKeepOldLogData);
			deleteOldLogs(lastDateToKeepLogData);
			//ToDo check for if there are error conditons and set flag to false
			Utils.printInConsole("maxDaysToKeepOldLogData : " + maxDaysToKeepOldLogData + "Date : "+ lastDateToKeepLogData + "deleteOldTables : "+ deleteOldLogs);
			deleteOldLogs = false;
		}

	}
	private void deleteOldLogs(LocalDate lastDateToKeepLogData) {

		int queryResult = materialLogsRepository.deleteByDate(lastDateToKeepLogData );
		Utils.printInConsole("Delete Query Result : " + queryResult);
			
		
		List<MaterialLogs> Logs = (List<MaterialLogs>)materialLogsRepository.selectLogsBeforeDate(lastDateToKeepLogData );
		if(Logs != null)
		{
			if(Logs.size() == 0) {
				Utils.printInConsole("No Records found");
			}
			for(int j=0;j<Logs.size();j++)
			{
				Utils.printInConsole("Date : "+ Logs.get(j).getDate() + "Result : "+ Logs.get(j).getResult());
			}
		}
		else {
			Utils.printInConsole("Logs null");
		}
	}

	private LocalDate getLastDateToKeepLogData(int daysToKeepOldLogData2) {
		LocalDate LastDate = LocalDate.now().minusDays(maxDaysToKeepOldTables);
		return LastDate;
	}

	@SuppressWarnings("unchecked")
	private void deleteOldLocationTable(LocalDate lastDateToKeepLocationTable) {

		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		String lastDate = lastDateToKeepLocationTable.format(formatters);
		List<String> tableNames = (List<String>)tableutil.SelectTableNamesLike(getMaterialsTableName(), lastDate );
		if(tableNames != null)
		{
			for(int j=0;j<tableNames.size();j++)
			{
				Utils.printInConsole("In Between chrono Table Names : " + tableNames.get(j) + "last date : "+ lastDateToKeepLocationTable.toString());
				tableutil.dropLocationTable(tableNames.get(j));
			}
		}
	}

	private LocalDate getLastDateToKeepLocationTable(int maxDaysToKeepOldTables) {

		LocalDate LastDate = LocalDate.now().minusDays(maxDaysToKeepOldTables);

		return LastDate;
	}
	private String getMaterialsTableName() {
		return getTableName("Materials");
	}

	private String getTableName(String tableName) {
		String currentTableName = "";
		String profileName = tableName + "_Location";

		currentTableName = profileName + "_";

		return currentTableName;
	}
}
