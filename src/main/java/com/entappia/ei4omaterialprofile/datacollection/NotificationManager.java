package com.entappia.ei4omaterialprofile.datacollection;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4omaterialprofile.constants.AppConstants;
import com.entappia.ei4omaterialprofile.constants.AppConstants.AssignmentType;
import com.entappia.ei4omaterialprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4omaterialprofile.udp.client.UdpClient;
import com.entappia.ei4omaterialprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4omaterialprofile.utils.Utils;
import com.entappia.ei4omaterialprofile.datacollection.Notification;
import com.entappia.ei4omaterialprofile.dbmodels.Notifications;
import com.entappia.ei4omaterialprofile.models.NotificationDetailMessages;
import com.entappia.ei4omaterialprofile.repository.NotificationsRepository;

@Component
public class NotificationManager {
	
	private UdpClient mUdpClient = null;
	@Autowired
	private NotificationsRepository notificationRepository;
	
	ArrayList<Notification> notificationList = new ArrayList<>();
	
	int intervalBetweenNotifications = AppConstants.intervalbetweenNotifications;//Default in minutes
	int maxNotificationCount = AppConstants.maxNotificationCount;// Default
	int minutesToKeepPreviousNotification = AppConstants.minutesToKeepPreviousNotification;//Default
	
	/*@Autowired
	public NotificationManager() {
	}*/
	@Autowired
	public NotificationManager(UdpIntegrationClient udpClient) {
		this.mUdpClient = udpClient;
		Utils.printInConsole("Inside Notification Manager Constructor");
	}
	
	public void setIntervalBetweenNotifications(int intervalBetweenNotifications) {
		this.intervalBetweenNotifications = intervalBetweenNotifications;
	}


	public void setMaxNotificationCount(int maxNotificationCount) {
		this.maxNotificationCount = maxNotificationCount;
	}

	//orderId, tagId, "ShippingArea",NotificationType.EMERGENCY, "", zoneName
	public void addNotification(String workOrderId, String tagId, String sNotification, NotificationType errorType, String zoneName)
	{
		boolean notificationFoundInList =  false;
		for(int i = 0; i < notificationList.size();i++)
		{
			Notification notification = notificationList.get(i);
		
			if(notification.getWorkOrderId().equals(workOrderId) && notification.getTagId().equals(tagId) 
					&& notification.getNotification().equals(sNotification)) {
				notificationFoundInList = true;
				notification.setErrorType(errorType);
				notification.setLastOccuredTime(new Date());
				notification.setSendNotification(true);
				notification.setZoneName(zoneName);
				//The last Notified Time and the Notification count is updated only during the 
				//time of sending notifications
			
		}
		}
		if(notificationFoundInList == false)
		{
			Notification notification = new Notification();
			notification.setWorkOrderId(workOrderId);
			notification.setTagId(tagId);
			notification.setNotification(sNotification);
			notification.setErrorType(errorType);
			notification.setLastOccuredTime(new Date());
			notification.setSendNotification(true);
			notification.setZoneName(zoneName);
			notificationList.add(notification);
		}
		
	}
	public void removeNotification(String workOrderId, String tagId, String sNotification)
	{
		for(int i = notificationList.size() - 1; i >= 0;i--)
		{
			Notification notification = notificationList.get(i);
		
			if(notification.getWorkOrderId().equals(workOrderId) && notification.getTagId().equals(tagId) 
					&& notification.getNotification().equals(sNotification))  {
				
				notificationList.remove(i);
			}
			
		}
	}
	public void sendNotification()
	{

		HashMap<String, List<HashMap<String, String>>> lnotificationShippingAreaList = new HashMap<>();
		HashMap<String, List<HashMap<String, String>>> lnotificationLimitCrossedList = new HashMap<>();
		

		NotificationType notificationType = NotificationType.INFORMATION;

		for(int i = 0; i < notificationList.size();i++)
		{
			Notification notification = notificationList.get(i);

			if(notification.isSendNotification() == true) {

				//check here for time expire and count for sending notification

				if(canNotify(notification) ==  true ) {
					
					notification.setSendNotification(true);
					notification.setLastNotifiedTime(new Date());
					notification.setNotificationCount(notification.getNotificationCount() + 1);
					
					if(notification.getNotification().equalsIgnoreCase("ShippingArea")){

						HashMap<String, String> notificationmap = new HashMap<>();

						notificationmap.put("workOrderId", notification.getWorkOrderId());
						notificationmap.put("tagId", notification.getTagId());
						notificationmap.put("zoneName", notification.getZoneName().toString());
						notificationmap.put("type", notification.getErrorType().toString());
						notificationmap.put("notificationCount", String.valueOf(notification.getNotificationCount()));
						
						Date endDate = new Date();
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
						String sEndDate = formatter.format(endDate);
						notificationmap.put("time", sEndDate );
						
						if(lnotificationShippingAreaList.containsKey(notification.getWorkOrderId()))
						{
							List<HashMap<String, String>> mapList = lnotificationShippingAreaList.get(notification.getWorkOrderId());

							if(mapList==null)
								mapList = new ArrayList<>();

							mapList.add(notificationmap);
							lnotificationShippingAreaList.put(notification.getWorkOrderId(), mapList); 

						}else
						{
							List<HashMap<String, String>> mapList= new ArrayList<>();
							mapList.add(notificationmap);
							lnotificationShippingAreaList.put(notification.getWorkOrderId(), mapList); 

						}

					}
					if(notification.getNotification().equalsIgnoreCase("Limitcrossed")){

						HashMap<String, String> notificationmap = new HashMap<>();

						notificationmap.put("workOrderId", notification.getWorkOrderId());
						notificationmap.put("tagId", notification.getTagId());
						notificationmap.put("zoneName", notification.getZoneName().toString());
						notificationmap.put("type", notification.getErrorType().toString());
						notificationmap.put("notificationCount", String.valueOf(notification.getNotificationCount()));
						
						Date endDate = new Date();
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
						String sEndDate = formatter.format(endDate);
						notificationmap.put("time", sEndDate );
						
						if(lnotificationLimitCrossedList.containsKey(notification.getWorkOrderId()))
						{
							List<HashMap<String, String>> mapList = lnotificationLimitCrossedList.get(notification.getWorkOrderId());

							if(mapList==null)
								mapList = new ArrayList<>();

							mapList.add(notificationmap);
							lnotificationLimitCrossedList.put(notification.getWorkOrderId(), mapList); 

						}else
						{
							List<HashMap<String, String>> mapList= new ArrayList<>();
							mapList.add(notificationmap);
							lnotificationLimitCrossedList.put(notification.getWorkOrderId(), mapList); 

						}

					}
					
					

					if( notification.getErrorType().compareTo(notificationType) > 0) {
						notificationType = notification.getErrorType();
					}
				}


				//The last Notified Time and the Notification count is updated only during the 
				//time of sending notifications

			}
			else
			{
				notification.setSendNotification(false);
			}

		}

		if (lnotificationShippingAreaList.size() > 0) {

			JSONObject mJSONArray = new JSONObject(lnotificationShippingAreaList);

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", 200);
			jsonObject.put("type", "ShippingArea");
			jsonObject.put("data", mJSONArray);
			mUdpClient.sendMessage(jsonObject.toString());

			NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
			notificationDetailMessages.setType("ShippingArea");
			notificationDetailMessages.setTagData(lnotificationShippingAreaList);

			Notifications notifications = new Notifications();
			notifications.setType(notificationType);
			notifications.setSource("material_profile");

			
			//Here frame message for chrono and non chrono separately check inner map also 
			Integer sum = lnotificationShippingAreaList.values().stream().mapToInt(List::size).sum();
			//change to sum and check inner values upto three
			//if(lnotificationTagsList.size()>1)
			if(sum > 1)
			{
				notifications.setMessage("Please remove " + sum + " tags in ShippingArea");
				notifications.setMessageObj(notificationDetailMessages);
			}
			else{  


				Map.Entry<String, List<HashMap<String, String>>> entry = lnotificationShippingAreaList.entrySet().iterator().next();

				List<HashMap<String, String>> value = entry.getValue();
				if(value.size()>1) {
					notifications.setMessage("Please remove " + value.size() + " tags in ShippingArea");
					notifications.setMessageObj(notificationDetailMessages);
				}else
				{
					notifications.setMessage("Please remove the tag: " + value.get(0).get("tagId") + " of work order : " + value.get(0).get("workOrderId") + " in zone : "
							+ value.get(0).get("zoneName"));
					notifications.setMessageObj(null);
				}
			}

			notifications.setNotifyTime(new Date());
			notificationRepository.save(notifications);
		}
		if (lnotificationLimitCrossedList.size() > 0) {

			JSONObject mJSONArray = new JSONObject(lnotificationLimitCrossedList);

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", 200);
			jsonObject.put("type", "limitCrossed");
			jsonObject.put("data", mJSONArray);
			mUdpClient.sendMessage(jsonObject.toString());

			NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
			notificationDetailMessages.setType("limitCrossed");
			notificationDetailMessages.setTagData(lnotificationLimitCrossedList);

			Notifications notifications = new Notifications();
			notifications.setType(notificationType);
			notifications.setSource("material_profile");

			
			//Here frame message for chrono and non chrono separately check inner map also 
			Integer sum = lnotificationLimitCrossedList.values().stream().mapToInt(List::size).sum();
			//change to sum and check inner values upto three
			//if(lnotificationTagsList.size()>1)
			if(sum > 1)
			{
				notifications.setMessage("Max limit crossed in "+ sum + " zones ");
				notifications.setMessageObj(notificationDetailMessages);
			}
			else{  


				Map.Entry<String, List<HashMap<String, String>>> entry = lnotificationLimitCrossedList.entrySet().iterator().next();

				List<HashMap<String, String>> value = entry.getValue();
				if(value.size()>1) {
					notifications.setMessage("Max limit crossed in  " + value.size() + " zones ");
					notifications.setMessageObj(notificationDetailMessages);
				}else
				{
					notifications.setMessage("Max limit crossed in zone " + value.get(0).get("zoneName") + " of work order : " + value.get(0).get("workOrderId"));
					notifications.setMessageObj(null);
				}
			}

			notifications.setNotifyTime(new Date());
			notificationRepository.save(notifications);
		}
		
		removeExpiredNotifications();

	}

	private void removeExpiredNotifications() {
		for(int i = notificationList.size() - 1; i >= 0;i--)
		{
			Notification notification = notificationList.get(i);
			Date lastNotifiedTime = notification.getLastNotifiedTime();
			Date currentTime = new Date();
			long duration = currentTime.getTime() - lastNotifiedTime.getTime();

			long diffMinutes = duration / (60 * 1000) % 60; 
			
			if(diffMinutes > minutesToKeepPreviousNotification) {
				
				notificationList.remove(i);
			}
			
		}
	}

	private boolean canNotify(Notification notification) {
	
		//Check all the possible conditions to send notification and returns
		//whether particular notification can be sent or not
		if(notification.getLastNotifiedTime() != null)
		{
		Date lastNotified = notification.getLastNotifiedTime();
		Date currentDate = new Date();
		long diff = currentDate.getTime() - lastNotified.getTime();
		
		//long diffSeconds = diff / 1000 % 60;  
		long diffMinutes = diff / (60 * 1000) % 60; 
		//long diffHours = diff / (60 * 60 * 1000);
		
		if(notification.getNotificationCount() < maxNotificationCount &&  diffMinutes >= intervalBetweenNotifications)
		{
			return true;
		}
		}
		else 
		{
			return true;
		}
		return false;
	}

	public void setUdpClient(UdpIntegrationClient udpClient) {
		
		this.mUdpClient = udpClient;
	}
	
	

}
